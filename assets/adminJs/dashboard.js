$(function () {
    loaddataDashboard();
})


//load data for dashboard
function loaddataDashboard() {
    $.ajax({
        url: dashboard_Api,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + localStorage.admin_token,
            "content-type": "application/json"
        }
    }).done(function (data) {
        var data_get = ["member_count","city_count","recommend_count","new_members_count"]
        for(var j = 0 ;j < 4;j++){
            $(".total"+j).text(data[data_get[j]]);
            $(".progress-bar").eq(j).css("width" , (data[data_get[j]]/100)*100 + "%")
            $(".percent" + j).text((data[data_get[j]]/100)*100 + " %");
        }

    }).fail(function (data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key];
        }
        $("#errormsg").text(errtext);
        errmsg();
    })

    $.ajax({
        url: recommendedUser_Api,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + localStorage.admin_token,
            "content-type": "application/json"
        }
    }).done(function (data) {
        $("#listUser").empty();
        if (data.length != 0) {
            for (var i = 0; i < data.length; i++) {
                $("#listUser").append(`<tr><td>${i + 1}</td>
                <td>${data[i].user.first_name + " " + data[i].user.last_name}</td>
                <td>${data[i].name}</td>
                <td>${data[i].user.email}</td>
                <td>${data[i].user.mobile}</td>
                <td>
                    <center><span class="m-switch m-switch--outline m-switch--icon m-switch--danger">
                            <label class="mb0">
                                <input onchange="activate_func(this)" type="checkbox" checked="checked" name="">
                                <span></span>
                            </label>
                        </span>
                    </center>
                </td>
            </tr>`);

            }

        } else {
            $("#listUser").append(`<img src="assets/images/nodata-found.png">`)
        }
    }).fail(function (data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key];
        }
        $("#errormsg").text(errtext);
        errmsg();
    })
}