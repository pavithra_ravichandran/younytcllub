$(function(){
    loadRecommendation(0)
});


function loadRecommendation(type){
    (type == 0) ? url = recommendation_Api: url = recommendation_Api + "?search=" + $(".searchinput").val();
    $(".findalldr").hide();
    $(".findallbtn").attr("disabled", false);
    $.ajax({
        url: url,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + localStorage.admin_token,
            "content-type": "application/json"
        }
    }).done(function(data) {
        $("#tableRecommend").empty();
        if (data.length != 0) {
            for (var i = 0; i < data.length; i++) {
                $("#tableRecommend").append(`<tr><td>${i+1}</td>
                <td>${data[i].user.first_name + " " +  data[i].user.last_name}</td>
                <td>${data[i].name}</td>
                <td>${data[i].user.email}</td>
                <td>${data[i].user.mobile}</td>
                <td>
                    <center><span class="m-switch m-switch--outline m-switch--icon m-switch--danger">
                            <label class="mb0">
                                <input onchange="activate_func(this)" id="${data[i].user._id}" type="checkbox" checked="checked" name="">
                                <span></span>
                            </label>
                        </span>
                    </center>
                </td>
            </tr>`);
            }
            (localStorage.used_id == "1") ? "" : $(".hide_del").hide();
        } else {
            $(".categoryhead").hide();
            $('.emptyimgappend').empty().append(`<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>`);
        }
    }).fail(function(data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key];
        }
        $("#errormsg").text(errtext);
        errmsg();
    })
}

