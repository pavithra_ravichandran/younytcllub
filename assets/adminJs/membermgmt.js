//the type 0 indicates the initial load and 1 is for sera

$(function () {
    loadcities();
    loadUserlist();//function to load results
})

//function load starts here 
function loadcities(){
        $.ajax({
            url: cities_Api,
            type: "GET",
            headers: {
                "Authorization": "Bearer " + localStorage.admin_token,
                "content-type": "application/json"
            }
        }).done(function (data) {
            $("#selectCity").empty();
            for(var i = 0;i<data.length;i++){
                $("#selectCity").append(`<option lat-val = "${data[i].latitude}" long-val = "${data[i].longitude}">${data[i].name}</option>`);
            }
        }).fail(function(data){
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key];
            }
            $("#errormsg").text(errtext);
            errmsg();
        })

}
//function ends here 

//function starts here 
function loadUserlist() {
    $.ajax({
        url: usermanagement_Api + 'is_active=&q=' + $(".searchinput").val(),
        type: "POST",
        headers: {
            "Authorization": "Bearer " + localStorage.admin_token,
            "content-type": "application/json"
        }
    }).done(function (data) {
        $("#listUser").empty();
        if (data.length != 0) {
            for (var i = 0; i < data.length; i++) {
                $("#listUser").append(`<div class="col-lg-3">
            <div class="epinsection">
                <img src="${(data[i].image == null)? "assets/images/dummy.png"  : data[i].image}" class="img-responsive userimage">
                <p class="epinusername">${data[i].first_name + " " + data[i].last_name}</p>
                <p class="epinuser"><i class="fa fa-phone"></i>&nbsp;&nbsp;<a href="tel:${data[i].mobile}">${data[i].mobile}</a></p>
                <p class="epinuser"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:${data[i].email}">${data[i].email}</a></p>
                <p class="epinuser"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;${(data[i].address.address == null ? "" : data[i].address.address) + (data[i].address.city == null ? "" : ', ' + data[i].address.city)  + (data[i].address.state == null ?  "" : ', ' + data[i].address.state)}</p>
                <p align="center">
                    <a href="#" class="editbtn m-portlet__nav-link btn m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#editdetailsmodal"><i class="la la-eye"></i></a><a href="#" del-id=${data[i]._id} class="deletebtn m-portlet__nav-link btn m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#deletedetailsmodal"><i class="la la-trash"></i></a></p>
            </div>
        </div>`);

            }

        } else {
            $("#listUser").append(`<img src="assets/images/nodata-found.png">`)
        }
    }).fail(function (data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key];
        }
        $("#errormsg").text(errtext);
        errmsg();
    })
}


//add the user module
function userAddmodule() {

    for (var i = 0; i < $("#addNewmember input").length; i++) {
        if ($("#addNewmember input").eq(i).val() == "") {
            $("#addNewmember input").eq(i).addClass("errorBorder");
            $("#errormsg").text($("#addNewmember input").eq(i).attr("placeholder") + " is required");
            iperr();
            event.stopPropagation();
            return;
        }
    }

    var postData = JSON.stringify({
        "first_name": $("#addNewmember input").eq(0).val(),
        "last_name" : "user",
        "email": $("#addNewmember input").eq(2).val(),
        "mobile": $("#addNewmember input").eq(1).val(),
        "username": $("#addNewmember input").eq(3).val(),
        "password": $("#addNewmember input").eq(4).val(),
        "address" : `{"city":"${$("#selectCity").val()}","latitude":"${$("#selectCity :checked").attr("lat-val")}","longitude":"${$("#selectCity :checked").attr("long-val")}"}`
    })

    $(".loaderbtn").html('Processing... <i class="fa fa-spinner fa-spin"></i>');
    $.ajax({
        url: userregisteration_Api,
        data: postData,
        type: "POST",
        headers: {
            'content-type': "application/json",
            'Authorization': "Bearer " + localStorage.admin_token
        }
    }).done(function (data) {
        $("#addNewmember input").val("");
        $("#succsmsg").text("user added successfully!!!");
        successmsg();
        $(".loaderbtn").html('Add New Member');



        $("#listUser").append(`<div class="col-lg-3">
        <div class="epinsection">
            <img src="${(data.image == null)? "assets/images/dummy.png"  : data.image}" class="img-responsive userimage">
            <p class="epinusername">${data.first_name + " " + data.last_name}</p>
            <p class="epinuser"><i class="fa fa-phone"></i>&nbsp;&nbsp;<a href="tel:${data.mobile}">${data.mobile}</a></p>
            <p class="epinuser"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:${data.email}">${data.email}</a></p>
            <p class="epinuser"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;${data.address + ', ' + data.city + ', ' + data.state}</p>
            <p align="center">
                <a href="#" class="editbtn m-portlet__nav-link btn m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#editdetailsmodal"><i class="la la-eye"></i></a><a href="#" class="deletebtn m-portlet__nav-link btn m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#deletedetailsmodal"><i class="la la-trash"></i></a></p>
        </div>
    </div>`);
    }).fail(function (data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key][0];
        }
        $("#errormsg").text(errtext);
        errmsg();
        $(".loaderbtn").html('Add New Member');
    })
}


//delete function starts here 
function deleteUser(userId){
    $.ajax({
        url: deleteUser_Api + '/' + userId,
        type: "DELETE",
        headers: {
            "content-type": "application/json",
            'Authorization': "Bearer " + localStorage.admin_token
        }
    }).done(function(data) {
       $(".deletebtn[del-id = "+userId+"]").remove();
    }).fail(function(data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key];
        }
        $("#errormsg").text(errtext);
        errmsg();
    })
}