//success toast fn starts here
function successmsg() {
    document.getElementById("succsmsg").className = "show";
    setTimeout(function() {
        document.getElementById("succsmsg").className = document.getElementById("succsmsg").className.replace("show", "");
    }, 4000);
}
//failure toast fn starts here
function errmsg() {
    document.getElementById("errormsg").className = "show";
    setTimeout(function() {
        document.getElementById("errormsg").className = document.getElementById("errormsg").className.replace("show", "");
    }, 3000);
}
//error toast for ip errors
function iperr() {
    var x = document.getElementById("errormsg");
    document.getElementById("errormsg").className = "show";
    setTimeout(function() {
        document.getElementById("errormsg").className = document.getElementById("errormsg").className.replace("show", "");
    }, 3000);
}


$(function() {
    $("body").append(`<div id="succsmsg"></div><div id="errormsg"></div>`);
    $("input").keyup(function(){
        $(this).removeClass("errorBorder");
    })
});


//logout function starts here 
function logoutFunc(){
    $.ajax({
        url: logout_Api,
        type: 'GET',
        headers: {
          "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + localStorage.admin_token)
        },
        success: function(data) {
          sessionStorage.clear();
          localStorage.clear();
          window.location.href ="index.html";
        },
        error: function(data) {
          sessionStorage.clear();
          localStorage.clear();
          window.location.href ="index.html";
        }
      })
}



//function activate_func starts here
function activate_func(me){
    var postData = JSON.stringify({
        "is_active": $(me).is(":checked")
    })
    $.ajax({
        url: activateUser_Api + '/' + $(me).attr("id"),
        type: "PATCH",
        data:postData,
        headers: {
            "Authorization": "Bearer " + localStorage.admin_token,
            "content-type": "application/json"
        }
    }).done(function(data) {
        (data.is_active) ? $(me).parents("tr").removeClass("inactive") : $(me).parents("tr").addClass("inactive")
       
    }).fail(function(data) {
        var errtext = "";
        for (var key in JSON.parse(data.responseText)) {
            errtext = JSON.parse(data.responseText)[key];
        }
        $("#errormsg").text(errtext);
        errmsg();
    })
}