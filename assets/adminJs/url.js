var domain = 'https://younyt-backend.herokuapp.com/'

//authentication modules starts here
 var login_Api = domain + 'login/';
 var changepass_Api = domain + 'change-password/';
 var logout_Api = domain + 'user/logout';

//dashboard api
var dashboard_Api = domain + 'dashboard';
var recommendedUser_Api = domain + 'recommendated/users';

//user management starts here
var userregisteration_Api = domain + 'register';
var usermanagement_Api = domain + 'user/filter?';
var cities_Api = domain + 'cities';
//recommendation starts here
var recommendation_Api = domain + 'recommendation';


//user activate deactivate Api starts here 
var activateUser_Api = domain + 'user/status';
var deleteUser_Api = domain + 'user/delete';