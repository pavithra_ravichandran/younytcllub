//login functionality starts here

function signIn() {
for(var i = 0;i< $("#loginId input").length;i++){
    if($("#loginId input").eq(i).val() == ""){
        $("#loginId input").eq(i).addClass("errorBorder");
        $("#errormsg").text($("#loginId input").eq(i).attr("placeholder") + " is required");
        iperr();
        event.preventDefault();
        return;
    }
}

$(".loaderbtn").html('Please Wait <i class="fa fa-spinner fa-spin"></i>');

var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({

        "username": $('#loginId input').eq(0).val(),
        "password": $('#loginId input').eq(1).val(),
        "client": uniqueclient

    });

    $.ajax({
        url: login_Api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $("#succsmsg").text("login successfully!!!");
            successmsg();
            localStorage.admin_token = data.token;
            localStorage.logindetails = JSON.stringify(data);
            setTimeout(function(){
                window.location.href ="dashboard.html"
            },3000);
            $(".loaderbtn").html('Sign In');
        },
        error: function(data) {
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#errormsg").text(errtext);
            errmsg();
            $(".loaderbtn").html('Sign In');
        }
    })
}

//login functionality ends here


//forgt password starts here
function requestPass(){
    for(i=0;i<$("#frgtPassword input").length;i++){
        if($("#frgtPassword input").eq(i).val() == ""){
            $("#frgtPassword input").eq(i).addClass("errorBorder");
            $("#errormsg").text($("#frgtPassword input").eq(i).attr("placeholder") + " is required");
            iperr();
            event.preventDefault();
            return;
        }
    }

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "email": $('#useremailfp').val(),
        "role" : 1
    });

    $.ajax({
        url: forgotpwd_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".fpBtn").html('Submit');
            $(".fpBtn").attr("disabled", false);
        },
        error: function(data) {

            $(".fpBtn").html('Submit');
            $(".fpBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    })
}

//change password starts here 
function changepassword(){
    for(i=0;i<$("#changePassword input").length;i++){
        if($("#changePassword input").eq(i).val() == ""){
            $("#changePassword input").eq(i).addClass("errorBorder");
            $("#errormsg").text($("#changePassword input").eq(i).attr("placeholder") + " is required");
            iperr();
            event.preventDefault();
            return;
        }
    }

    $(".loaderbtn").html('Please Wait <i class="fa fa-spinner fa-spin"></i>');

    var postdata = JSON.stringify({
        "old_password": $("#changePassword input").eq(1).val(),
        "new_password": $("#changePassword input").eq(2).val()
      });
    
      $.ajax({
        url: changepass_Api,
        type: 'post',
        data: postdata,
        headers: {
          "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Bearer " + localStorage.admin_token)
        },
        success: function(data) {
    
            $("#succsmsg").text("Password changed successfully!!!");
            successmsg();
            setTimeout(function(){
                window.location.href ="dashboard.html"
            },3000);
            $(".loaderbtn").html('Submit');

        },
        error: function(data) {
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#errormsg").text(errtext);
            errmsg();
             $(".loaderbtn").html('Submit');
        }
      })
}